# Interface pour ordinateur de poche SHARP

Cet interface a pour but de remplacer les périphériques imprimantes cassettes tels que le CE-125 ou CE-126P pour les ordinateurs de poche SHARP modèles PC-125x, PC-126x, PC-14xx ou PC-E500.

Il permet d'effectuer un transfert des programmes entre un SHARP et un ordinateur de bureau via les ports audio. Couplé à une carte microcontrôleur Arduino, il permet d'utiliser les fonctions d'impression du SHARP pour les rediriger vers la sortie série de l'Arduino. Il est ainsi possible d'afficher les données émises sur un ordinateur ou de les imprimer avec une imprimante disposant d'une entrée série TTL.

![](Schematic_Sharp_connector_2022-12-30.png)

## Liste des composants

* 1 × Condensateur 100nF : C1 
* 1 × Condensateur 100µF : C2 
* 3 × diodes de signal 1N4148 : D1,D2,D3
* 1 × LED 5mm
* 1 × Porte logique Non [MM74C14N](https://www.aliexpress.com/item/4000980619379.html) ou HEF4069 : U1
* 7 × résistances 10kΩ : R2,R3,R4,R5,R7,R8,R9
* 1 × résistance 47k : R6
* 1 × résistance 4.7MΩ : R10
* 1 × résistance 800Ω : R11
* 2 × [connecteurs jack 3.5mm](https://www.aliexpress.com/item/32871877936.html)

## Connecteurs

L'ordinateur de poche ne doit être allumée qu'une fois l'ensemble des connexions nécessaires réalisées (y compris la prise jack utilisée).

### Alimentation
Connecter à une alimentation régulée entre 5.5 et 6.5 V. La LED s'allume pour indiquer la connexion. La diode D1 sert de protection contre l'inversion de polarité.

### Sharp
Se connecte à l'interface 11 broches de l'ordinateur de poche.

### Arduino
Connecter aux broches correspondante d'un Arduino UNO pour l'utilisation des fonctions d'impression du SHARP. La carte Arduino peut être alimentée par la broche Vin.

### Entrée-Sortie Audio
Prises femelles jack 3.5 mm à connecter avec des prises mono ou stéréo aux entrées correspondantes sur une carte son (microphone pour la sauvegarde ou casque pour le chargement de programmes).

## Utilisation
### Sauvegarde de programmes
#### SHARP → PC
L'interface doit être connectée à l'ordinateur sur l'entrée microphone. Un logiciel tel qu'[Audacity](https://www.audacityteam.org/) peut être utilisé pour l'enregistrement des programmes.

Paramètres recommandés :

* Entrée microphone : niveau de base (non amplifiée) : en général 10 % sur les cartes son intégrées, jusqu'à 40 % sur les cartes externes audio-USB.
* Canaux : Mono
* Taux d’échantillonnage : 44100 Hz.
* Format d'enregistrement : Flac level 8, 16 bits.

Sur le SHARP :

* Sauvegarde de la zone programmes : `CSAVE`
* Sauvegarde d'une partie de la RAM : `CSAVE M` *adresse\_début,adresse\_fin*

Par exemple, pour une sauvegarde de l'intégralité de la RAM sur un PC-1262 :

	CSAVE M 16384,26340

#### PC → SHARP

Connecter l'interface à la sortie casque, volume à 80 %.

Sur le SHARP

* zone programmes : `CLOAD`
* zone RAM : `CLOAD M`

### Interface imprimante

L'interface imprimante SHARP pour Arduino UNO a été inventée par [Walter Fischer](http://www.cavefischer.at/spc/html/CE-126P_Emulator.html). Il a développé des programmes permettant au microcontrôleur de lire les caractères d'impression envoyés par l'ordinateur de poche et les rediriger vers la sortie série. Les versions de ses programmes disponibles ici ont été modifiées pour afficher les caractères spéciaux √ et π sur un terminal Linux en UTF-8.

![](printer.jpg)*Utilisation de l'interface avec une imprimante thermique série TTL*

