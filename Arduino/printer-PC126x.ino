// Emulator Sharp CE-126P für PC-1260

// http://www.cavefischer.at/spc/html/CE-126P_Emulator.html#PC-1260

// Post traitement Geany :
// Remplacer :
// \n → £
// £ *(\d+:) → \n\1
// puis divers ajustements

const int IN_SEL1 = 2; // Pin D2, SEL1 Sharp PC-1260, pin 11 der 11-pol. Buchsenleiste
const int IN_SEL2 = 3; // Pin D3, SEL2 Sharp PC-1260, pin 10 der 11-pol. Buchsenleiste
const int OUT_ACK = 4; // Pin D4, ACK  Sharp CE-126P, pin  9 der 11-pol. Buchsenleiste
const int IN_Dout = 8; // Pin D8, Dout Sharp PC-1260, pin  5 der 11-pol. Buchsenleiste
const int IN_Busy = 9; // Pin D9, Busy Sharp PC-1260, pin  4 der 11-pol. Buchsenleiste
const int InfoLED = 13;
boolean Busy;
boolean SEL1;
boolean SEL2;
int DataBit;
int DataByte;
long Timeout;
int i;

void setup()
    {
    Serial.begin(9600);
    pinMode(OUT_ACK, OUTPUT);
    pinMode(IN_Dout, INPUT);
    pinMode(IN_Busy, INPUT);
    pinMode(IN_SEL1, INPUT);
    pinMode(IN_SEL2, INPUT);
    pinMode(InfoLED, OUTPUT);
    Serial.println();
    Serial.println("Bereit:");
    Serial.println();
    }

void loop()
    {
    
    digitalWrite(InfoLED, LOW);
    
    SEL1 = digitalRead(IN_SEL1);
    SEL2 = digitalRead(IN_SEL2);
    
    if (SEL2 && SEL1) {
    // DeviceSelect:
    digitalWrite(InfoLED, HIGH);
    delayMicroseconds(50);
    i = 0;
    do {
    SEL1 = digitalRead(IN_SEL1);
    } while (SEL1);
    Timeout = millis();
    do {
    Busy = digitalRead(IN_Busy);
    if (millis() - Timeout > 50) break;
    } while (!Busy);
    delayMicroseconds(50);
    digitalWrite(OUT_ACK, HIGH);
    Timeout = millis();
    do {
    SEL1 = digitalRead(IN_SEL1);
    if (millis() - Timeout > 50) break;
    } while (!SEL1);
    delayMicroseconds(150);
    digitalWrite(OUT_ACK, LOW);
    do {
    SEL1 = digitalRead(IN_SEL1);
    } while (SEL1);
    delayMicroseconds(150);
    }
    
    Busy = digitalRead(IN_Busy);
    
    if (Busy) {
    // Daten:
    digitalWrite(InfoLED, HIGH);
    i = 0;
    DataByte = 0;
    do {
    do {
    Busy = digitalRead(IN_Busy);
    } while (!Busy);
    delayMicroseconds(500);
    DataBit = digitalRead(IN_Dout);
    digitalWrite(OUT_ACK, HIGH);
    do {
    Busy = digitalRead(IN_Busy);
    } while (Busy);
    delayMicroseconds(480);
    digitalWrite(OUT_ACK, LOW);
    DataByte = DataByte | (DataBit << i);
    i++;
    } while (i < 8);
    // Ausgabe:
    switch (DataByte) {
    case 13:
    Serial.println();
    break;
    case 91:
    Serial.print("√");
    break;
    case 93:
    Serial.print("π");
    break;
    default:
    if (DataByte > 31 && DataByte < 127) Serial.print(char(DataByte));
    //Serial.print(DataByte);Serial.print(" ");
    
    }
    }
    
    }
    
