// Emulator Sharp CE-126P für PC-1401, PC-1403 u. PC-E500

const int OUT_ACK = 4; // Pin D4, ACK  Sharp CE-126P, pin 9 der 11-pol. Buchsenleiste
const int IN_Xout = 6; // Pin D6, Xout Sharp PC-1401, pin 7 der 11-pol. Buchsenleiste
const int IN_Dout = 8; // Pin D8, Dout Sharp PC-1401, pin 5 der 11-pol. Buchsenleiste
const int IN_Busy = 9; // Pin D9, Busy Sharp PC-1401, pin 4 der 11-pol. Buchsenleiste
const int InfoLED = 13;
boolean Busy;
boolean Xout;
int DataBit;
int DataByte;
int i;

void setup() {
Serial.begin(9600);
pinMode(OUT_ACK, OUTPUT);
pinMode(IN_Xout, INPUT);
pinMode(IN_Dout, INPUT);
pinMode(IN_Busy, INPUT);
pinMode(InfoLED, OUTPUT);
Serial.println();
Serial.println("Bereit:");
Serial.println();
}

void loop() {

digitalWrite(InfoLED, LOW);

Xout = digitalRead(IN_Xout);

if (Xout) {
// DeviceSelect:
digitalWrite(InfoLED, HIGH);
delayMicroseconds(50);
i = 0;
do {
digitalWrite(OUT_ACK, HIGH);
do {
Busy = digitalRead(IN_Busy);
} while (!Busy);
delayMicroseconds(50);
digitalWrite(OUT_ACK, LOW);
do {
Busy = digitalRead(IN_Busy);
} while (Busy);
delayMicroseconds(150);
i++;
} while (i < 8);
do {
Xout = digitalRead(IN_Xout);
} while (Xout);
digitalWrite(OUT_ACK, HIGH);
delayMicroseconds(500);
digitalWrite(OUT_ACK, LOW);
}

Busy = digitalRead(IN_Busy);

if (Busy && !Xout) {
// Daten:
digitalWrite(InfoLED, HIGH);
i = 0;
DataByte = 0;
do {
do {
Busy = digitalRead(IN_Busy);
} while (!Busy);
delayMicroseconds(50);
DataBit = digitalRead(IN_Dout);
digitalWrite(OUT_ACK, HIGH);
do {
Busy = digitalRead(IN_Busy);
} while (Busy);
delayMicroseconds(50);
digitalWrite(OUT_ACK, LOW);
DataByte = DataByte | (DataBit << i);
i++;
} while (i < 8);
// Ausgabe:
switch (DataByte) {
case 13:
Serial.println();
break;
case 24:
Serial.print("√");
break;
case 25:
Serial.print("π");
break;
case 48:
Serial.print("O"); // Buchstabe "O"
break;
case 240:
Serial.print("0"); // Ziffer Null
break;
default:
if (DataByte > 31 && DataByte < 127) Serial.print(char(DataByte));
//Serial.print(DataByte);Serial.print(" ");
}
}

} // Ende loop

